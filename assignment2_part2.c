#include "assignment2_part2.h"

//Creates Queue
void createQueue(struct queue *q){
	q->timeQuantum=0;
	q->head=NULL;
	q->tail=NULL;
}

//Adds a process to the back of the queue
void enqueue(struct queue *q,int newBurstTime,char newName[20],int startTime,int seen){
	struct node* node;
	node=malloc(sizeof(struct node));
	strcpy(node->name,newName);
	node->burstTime=newBurstTime;
	node->startTime=startTime;
	node->seen=seen;
	node->next=NULL;
	node->prev=NULL;

	if(q->head==NULL){
		q->head=node;
	}
	else{
		node->prev=q->tail;
		q->tail->next=node;
	}

	q->tail=node;
}

//Fills the queue with a line from input file
//Strtok parses the string and separates it into tokens based on white space
void fillQueue(struct queue* q,char *line){
	char* pch;
	char *name;
	char *tempLine=(char *)malloc(256*sizeof(char));
	strcpy(tempLine,line);
	name=(char *)malloc(60*sizeof(char));
	int burst;
	pch=strtok(tempLine," ");
	pch=strtok(NULL," ");
	pch=strtok(NULL," ");
	pch=strtok(NULL," ");
	q->timeQuantum=atoi(pch);

	pch=strtok(NULL," ");
	if(pch!=NULL)
		strcpy(name,pch);
	while(pch!=NULL){
		pch=strtok(NULL," ");
		if(pch!=NULL)
			burst=atoi(pch);
		enqueue(q,burst,name,0,0);
		pch=strtok(NULL," ");
		if(pch!=NULL)
			strcpy(name,pch);
	}
}

//Checks if queue is empty
int isEmpty(struct queue *q){
	if(q->head==0)
		return 1;
	else return 0;
}

//Checks if a given process has another process which succeeds it
int hasNext(struct node* node1){
	if(node1->next==NULL)
		return 0;
	else return 1;
}

//Round robin dequeue, removes the process at front of queue
//Places it at back of queue is bursttime>time quantum
struct node* dequeueRR(struct queue* q){
	struct node* removed;
	if(isEmpty(q))
		return NULL;
	removed=q->head;
	if(hasNext(removed)){
		q->head=removed->next;
		q->head->prev=NULL;
	}
	else{
		q->head=NULL;
		q->tail=NULL;
	}

	if(removed->burstTime>q->timeQuantum){
		enqueue(q,removed->burstTime-q->timeQuantum,removed->name,removed->startTime,1);
	}

	return removed;
}

//Removes process at head of queue
struct node* dequeue(struct queue* q){
	struct node* removed;
	if(isEmpty(q))
		return NULL;
	removed=q->head;
	if(hasNext(removed)){
		q->head=removed->next;
		q->head->prev=0;
	}
	else{
		q->head=NULL;
		q->tail=NULL;
	}

	return removed;
}

//Frees memory allocated by queue
void destroyQueue(struct queue *q){
	while(!isEmpty(q)){
		dequeue(q);
	}
	free(q);

}

//Removes process with shortest burst time
struct node* shortestBurst(struct queue* q){
	struct node* tempNode;
	tempNode=malloc(sizeof(struct node));
	tempNode->burstTime=999999;
	struct node* currentNode=malloc(sizeof(struct node));
	currentNode=q->head;

	while(currentNode!=NULL){
		if(currentNode->burstTime<tempNode->burstTime)
			tempNode=currentNode;
		currentNode=currentNode->next;
	}
	if(tempNode->prev!=NULL)
		tempNode->prev->next=tempNode->next;
	else
		q->head=tempNode->next;

	if(tempNode->next!=NULL)
		tempNode->next->prev=tempNode->prev;
	else
		q->tail=tempNode->prev;


	return tempNode;

}

//Prints the order of execution in a FCFS scheduler
void printQueueNames(struct queue *queue,FILE **writeFile,int x){
	printf("Ready Queue %d Applying FCFS Scheduling: \n\n",x);
	printf("Order of selection by CPU:\n");
	fprintf(*writeFile,"Ready Queue %d Applying FCFS Scheduling: \n\n",x);
	fprintf(*writeFile,"Order of selection by CPU:\n");
	while(isEmpty(queue)==0){
		struct node *node1=dequeue(queue);
		printf("%s ",node1->name);
		fprintf(*writeFile,"%s ",node1->name);
	}
	printf("\n\n");
	fprintf(*writeFile,"\n\n");
}

//Prints wait times for each process in a FCFS scheduler
void printWaitTimes(struct queue *queue,FILE **writeFile){
	int sum=0;
	printf("Individual waiting times for each process:\n");
	fprintf(*writeFile,"Individual waiting times for each process:\n");
	while(isEmpty(queue)==0){
		struct node *node1=dequeue(queue);
		printf("%s = %d\n",node1->name,sum);
		fprintf(*writeFile,"%s = %d\n",node1->name,sum);
		sum+=node1->burstTime;
	}
	printf("\n");
	fprintf(*writeFile,"\n");
}

//Gets the number of processes in a given queue
int getNumProcesses(struct queue *queue){
	int num=0;
	while(isEmpty(queue)==0){
		dequeue(queue);
		num++;
	}
	return num;
}

//prints average wait time in FCFS scheduler
void printAvgWaitTime(struct queue *queue,FILE **writeFile,int numProcesses){
	float sum=0,wait=0;
	int i=0;
	int waitTimes[numProcesses];
	waitTimes[i]=wait;
	i++;
	while(isEmpty(queue)==0){
		struct node *node1=dequeue(queue);
		wait+=node1->burstTime;
		waitTimes[i]=wait;
		i++;
	}
	for(int j=0;j<numProcesses;j++)
		sum+=waitTimes[j];
	printf("Average waiting time = %2.1f\n\n",sum/numProcesses);
	fprintf(*writeFile,"Average waiting time = %2.1f\n\n",sum/numProcesses);
}

//Prints the order of execution in SJF scheduler
void printQueueSJF(struct queue *queue,FILE **writeFile,int x){
	printf("Ready Queue %d Applying SJF Scheduling:\n\n",x);
	printf("Order of selection by CPU:\n");
	fprintf(*writeFile,"Ready Queue %d Applying SJF Scheduling:\n\n",x);
	fprintf(*writeFile,"Order of selection by CPU:\n");

	while(isEmpty(queue)==0){
		struct node *node=shortestBurst(queue);
		printf("%s ",node->name);
		fprintf(*writeFile,"%s ",node->name);
	}
	printf("\n\n");
	fprintf(*writeFile,"\n\n");
}

//Prints wait times for each process in SJF scheduler
void printWaitTimesSJF(struct queue *queue,FILE **writeFile){
	int sum=0;
	printf("Individual waiting times for each process:\n");
	fprintf(*writeFile,"Individual waiting times for each process:\n");
	while(isEmpty(queue)==0){
		struct node *node1=shortestBurst(queue);
		printf("%s = %d\n",node1->name,sum);
		fprintf(*writeFile,"%s = %d\n",node1->name,sum);
		sum+=node1->burstTime;
		}
	printf("\n");
	fprintf(*writeFile,"\n");

}

//Prints average wait time in SJF scheduler
void printAvgWaitTimeSJF(struct queue *queue,FILE **writeFile,int numProcesses){
	float sum=0,wait=0;
	int i=0;
	int waitTimes[numProcesses];
	waitTimes[i]=wait;
	i++;
	while(isEmpty(queue)==0){
		struct node *node1=shortestBurst(queue);
		wait+=node1->burstTime;
		waitTimes[i]=wait;
		i++;
	}
	for(int j=0;j<numProcesses;j++)
		sum+=waitTimes[j];
	printf("Average waiting time = %2.1f\n\n",sum/numProcesses);
	fprintf(*writeFile,"Average waiting time = %2.1f\n\n",sum/numProcesses);
}

//Prints order of execution in RR scheduler
void printQueueRR(struct queue *queue,FILE **writeFile,int x){
	printf("Ready Queue %d Applying RR Scheduling: \n\n",x);
	printf("Order of selection by CPU:\n");
	fprintf(*writeFile,"Ready Queue %d Applying RR Scheduling: \n\n",x);
	fprintf(*writeFile,"Order of selection by CPU:\n");
	while(isEmpty(queue)==0){
		struct node *node1=dequeueRR(queue);
		printf("%s ",node1->name);
		fprintf(*writeFile,"%s ",node1->name);
	}
	printf("\n\n");
	fprintf(*writeFile,"\n\n");
}

//Prints turnaround time for each process in RR scheduler
void printTurnAroundTime(struct queue *queue,FILE **writeFile){
	int time=0,turnTime;
	queue->head->startTime=time;

	printf("Turnaround times for each process:\n");
	fprintf(*writeFile,"Turnaround times for each process:\n");

	while(isEmpty(queue)==0){
		struct node *node=dequeueRR(queue);
		if(node->burstTime>queue->timeQuantum){
			time+=queue->timeQuantum;
		}

		else if(node->burstTime<=queue->timeQuantum){
			time+=node->burstTime;
			turnTime=time-node->startTime;
			printf("%s = %d\n",node->name,turnTime);
			fprintf(*writeFile,"%s = %d\n",node->name,turnTime);
		}
		if(isEmpty(queue)==0 && queue->head->seen==0)
			queue->head->startTime=time;
	}

	printf("\n");
	fprintf(*writeFile,"\n");


}

//Master function which calls each Scheduling algorithm and refills the queue
void printAllInfo(struct queue *queue,char* line,FILE **writeFile,int x){
	printQueueNames(queue,writeFile,x);
	fillQueue(queue,line);
	printWaitTimes(queue,writeFile);
	fillQueue(queue,line);
	int num=getNumProcesses(queue);
	fillQueue(queue,line);
	printAvgWaitTime(queue,writeFile,num);
	fillQueue(queue,line);
	printQueueSJF(queue,writeFile,x);
	fillQueue(queue,line);
	printWaitTimesSJF(queue,writeFile);
	fillQueue(queue,line);
	printAvgWaitTimeSJF(queue,writeFile,num);
	fillQueue(queue,line);
	printQueueRR(queue,writeFile,x);
	fillQueue(queue,line);
	printTurnAroundTime(queue,writeFile);
}

