#include <stdio.h>
#include <stdlib.h>
#include<string.h>

#ifndef ASSIGNMENT2_PART2_H_
#define ASSIGNMENT2_PART2_H_

//Struct for a node in linked list, ie process. int variable seen represents if the current process
//has been replaced in the queue for the round robin scheduling
struct node{
	int burstTime;
	char name[20];
	int startTime;
	int seen;
	struct node *next;
	struct node *prev;
};

//Struct for queue
struct queue{
	int timeQuantum;
	struct node *head;
	struct node *tail;
};

//Function Declarations
void createQueue(struct queue *q);
void enqueue(struct queue *q,int newBurstTime,char newName[20],int startTime,int seen);
void fillQueue(struct queue* q,char *line);
int isEmpty(struct queue *q);
int hasNext(struct node* node1);
struct node* dequeueRR(struct queue* q);
struct node* dequeue(struct queue* q);
void destroyQueue(struct queue *q);
struct node* shortestBurst(struct queue* q);
void printQueueNames(struct queue *queue,FILE **writeFile,int x);
void printWaitTimes(struct queue *queue,FILE **writeFile);
int getNumProcesses(struct queue *queue);
void printAvgWaitTime(struct queue *queue,FILE **writeFile,int numProcesses);
void printQueueSJF(struct queue *queue,FILE **writeFile,int x);
void printAllInfo(struct queue *queue,char* line,FILE **writeFile,int x);
void printWaitTimesSJF(struct queue *queue,FILE **writeFile);
void printAvgWaitTimeSJF(struct queue *queue,FILE **writeFile,int numProcesses);
void printQueueRR(struct queue *queue,FILE **writeFile,int x);

#endif /* ASSIGNMENT2_PART2_H_ */
