#include "assignment2_part2.c"

int main(){
	FILE *fp=fopen("cpu_scheduling_input_file.txt","r");
	FILE *out=fopen("cpu_scheduling_output_file.txt","w");
	char line1[256]=" ";
	int numQueues=0,x=0;
	while(fgets(line1,sizeof(line1),fp)){
		numQueues++;
	}
	rewind(fp);

	char line[numQueues][256];

	//Array of queues to store all processes from input file
	struct queue **queue=malloc(numQueues*sizeof(struct queue));
	for(int p=0;p<numQueues;p++){
		queue[p]=malloc(sizeof(struct queue));
	}

	//Store all lines in the input file into an array of strings
	while(fgets(line[x],sizeof(line[x]),fp)){
		x++;
	}

	//Create and fill each queue
	for(int j=0;j<numQueues;j++){
		createQueue(queue[j]);
		fillQueue(queue[j],line[j]);
	}

	//Prints scheduling information for each queue
	for(int i=0;i<numQueues;i++){
		printAllInfo(queue[i],line[i],&out,i+1);
	}

	//Destroys each queue
	for(int i=0;i<numQueues;i++){
		destroyQueue(queue[i]);
	}
}





